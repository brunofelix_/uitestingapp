package com.brunofelixdev.uitestingapp.ui.fragment

import android.os.Bundle
import androidx.fragment.app.FragmentFactory
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.brunofelixdev.uitestingapp.R
import com.brunofelixdev.uitestingapp.model.DummyMovies.THE_RUNDOWN
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Test class fragment for MovieDetailFragment
 */
@LargeTest
@RunWith(AndroidJUnit4ClassRunner::class)
class MovieDetailFragmentTest {

    @Test
    fun test_isMovieDataVisible() {
        //  given
        val movie = THE_RUNDOWN
        val fragmentFactory = FragmentFactory()
        val bundle = Bundle()
        bundle.putInt("movie_id", movie.id)

        //  when
        val scenario = launchFragmentInContainer<MovieDetailFragment>(
            fragmentArgs = bundle,
            factory = fragmentFactory
        )

        //  then
        onView(withId(R.id.movie_title)).check(matches(withText(movie.title)))
        onView(withId(R.id.movie_description)).check(matches(withText(movie.description)))
    }
}