package com.brunofelixdev.uitestingapp.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.brunofelixdev.uitestingapp.R
import com.brunofelixdev.uitestingapp.databinding.ActivityHomeBinding
import com.brunofelixdev.uitestingapp.databinding.ActivitySecondBinding
import com.brunofelixdev.uitestingapp.ui.fragment.MovieDetailFragment

class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeViews()
    }

    private fun initializeViews() {
        binding = ActivityHomeBinding.inflate(layoutInflater).apply {
            setContentView(root)
        }

        if (supportFragmentManager.fragments.size == 0) {
            val movieId = 1
            val bundle = Bundle()
            bundle.putInt("movie_id", movieId)
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MovieDetailFragment::class.java, bundle)
                .commit()
        }
    }
}