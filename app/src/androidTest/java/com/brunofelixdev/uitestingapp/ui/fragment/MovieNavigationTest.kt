package com.brunofelixdev.uitestingapp.ui.fragment

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.brunofelixdev.uitestingapp.R
import com.brunofelixdev.uitestingapp.ui.activity.HomeActivity
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4ClassRunner::class)
class MovieNavigationTest {

    @Test
    fun test_movieNavigationFragment() {
        //  Launch Host Activity
        val activity = ActivityScenario.launch(HomeActivity::class.java)

        //  Nav to FragmentDirectors
        onView(withId(R.id.movie_directiors)).perform(click())

        //  Verify
        onView(withId(R.id.fragment_directors_parent)).check(matches(isDisplayed()))

        //  Press back button
        pressBack()

        //  Verify
        onView(withId(R.id.fragment_movie_ditail_parent)).check(matches(isDisplayed()))
    }
}