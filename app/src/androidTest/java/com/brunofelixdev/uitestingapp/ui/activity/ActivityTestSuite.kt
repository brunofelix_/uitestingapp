package com.brunofelixdev.uitestingapp.ui.activity

import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    FirstActivityTest::class,
    SecondActivityTest::class
)
class ActivityTestSuite