package com.brunofelixdev.uitestingapp.ui.activity

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.brunofelixdev.uitestingapp.R
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class SecondActivityTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(SecondActivity::class.java)

    @Test
    fun test_isActivityInView() {
        onView(withId(R.id.root_second)).check(matches(isDisplayed()))
    }

    @Test
    fun test_isTextViewTitleVisible() {
        onView(withId(R.id.tv_second_activity)).check(matches(isDisplayed()))
    }

    @Test
    fun test_isButtonBackVisible() {
        onView(withId(R.id.btn_back)).check(matches(isDisplayed()))
    }

    @Test
    fun test_isTextTitleDisplayed() {
        onView(withId(R.id.tv_second_activity)).check(matches(withText(R.string.txt_second_activity)))
    }
}