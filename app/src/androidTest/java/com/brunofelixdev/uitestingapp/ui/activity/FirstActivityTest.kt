package com.brunofelixdev.uitestingapp.ui.activity

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.brunofelixdev.uitestingapp.R
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Test class ui for FirstActivity
 */
@RunWith(AndroidJUnit4ClassRunner::class)
class FirstActivityTest {

    @Test
    fun test_isActivityInView() {
        val activity = ActivityScenario.launch(FirstActivity::class.java)

        onView(withId(R.id.root_first)).check(matches(isDisplayed()))
    }

    @Test
    fun test_visibilityTitleAndNextButton() {
        val activity = ActivityScenario.launch(FirstActivity::class.java)

        onView(withId(R.id.tv_first_activity)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_next)).check(matches(isDisplayed()))

        //  another way
        onView(withId(R.id.btn_next)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
    }

    @Test
    fun test_isTitleTextIsDisplayed() {
        val activity = ActivityScenario.launch(FirstActivity::class.java)

        onView(withId(R.id.tv_first_activity)).check(matches(withText(R.string.txt_first_activity)))
    }

    @Test
    fun test_navSecondActivity() {
        val activity = ActivityScenario.launch(FirstActivity::class.java)

        onView(withId(R.id.btn_next)).perform(click())

        onView(withId(R.id.root_second)).check(matches(isDisplayed()))
    }

    @Test
    fun test_backPressToFirstActivity() {
        val activity = ActivityScenario.launch(FirstActivity::class.java)

        onView(withId(R.id.btn_next)).perform(click())

        onView(withId(R.id.root_second)).check(matches(isDisplayed()))

        //  onView(withId(R.id.btn_back)).perform(click())

        pressBack()

        onView(withId(R.id.root_first)).check(matches(isDisplayed()))
    }
}