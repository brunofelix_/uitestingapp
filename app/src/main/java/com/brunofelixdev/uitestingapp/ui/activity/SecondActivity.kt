package com.brunofelixdev.uitestingapp.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.brunofelixdev.uitestingapp.databinding.ActivitySecondBinding

class SecondActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySecondBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySecondBinding.inflate(layoutInflater).apply {
            setContentView(root)
        }

        binding.btnBack.setOnClickListener {
            onBackPressed()
        }
    }
}