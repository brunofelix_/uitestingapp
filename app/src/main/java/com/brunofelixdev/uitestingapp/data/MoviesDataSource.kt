package com.brunofelixdev.uitestingapp.data

import com.brunofelixdev.uitestingapp.model.Movie

interface MoviesDataSource {
    fun getMovie(movieId: Int): Movie?
}