package com.brunofelixdev.uitestingapp.factory

import androidx.fragment.app.FragmentFactory
import com.brunofelixdev.uitestingapp.ui.fragment.DirectorsFragment
import com.brunofelixdev.uitestingapp.ui.fragment.MovieDetailFragment
import com.brunofelixdev.uitestingapp.ui.fragment.StarActorsFragment

class MovieFragmentFactory : FragmentFactory() {

    private val TAG: String = "AppDebug"

    override fun instantiate(classLoader: ClassLoader, className: String) =

        when (className) {

            MovieDetailFragment::class.java.name -> {
                MovieDetailFragment()
            }

            DirectorsFragment::class.java.name -> {
                DirectorsFragment()
            }

            StarActorsFragment::class.java.name -> {
                StarActorsFragment()
            }

            else -> {
                super.instantiate(classLoader, className)
            }
        }
}